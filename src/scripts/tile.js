export default class Tile {
  constructor (value, position) {
    this.value = value;
    this.position = position;
    this.previousPosition = null;
    this.merged = null;
  }

  updatePosition (newPosition) {
    this.position = newPosition;
  }

  saveCurrentPositionAsPrevious () {
    this.previousPosition = Object.assign({}, this.position);
  }

  positionEquals (position) {
    return this.position.x === position.x && this.position.y === position.y;
  }
}
