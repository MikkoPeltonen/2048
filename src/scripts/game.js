import "../styles/game.scss";

import Board from "./board";
import Grid from "./grid";
import Tile from "./tile";


export default class Game {
  static MoveDirection = {
    LEFT: 1,
    RIGHT: 2,
    UP: 3,
    DOWN: 4,
  };

  static DirectionVector = {
    [Game.MoveDirection.LEFT]:  { x: -1, y: 0  },
    [Game.MoveDirection.UP]:    { x: 0,  y: -1 },
    [Game.MoveDirection.RIGHT]: { x: 1,  y: 0  },
    [Game.MoveDirection.DOWN]:  { x: 0,  y: 1  },
  };

  constructor (container) {
    this.container = container;
    this.gameOver = false;
    this.size = 4;
    this.initialTiles = 2;
    this.historySize = 10;
    this.twoFourDistribution = 0.9;
    this.score = 0;

    this.setup();
  }

  setup () {
    this.history = [];
    this.grid = new Grid(this.size, null);
    this.board = new Board(this.container, this.size);

    this.addKeyBindings();
    this.addInitialTiles();

    this.board.performAction(this.grid);
  }

  addKeyBindings () {
    $(document).keyup(e => {
      let direction;
      if (e.which === 37) {
        direction = Game.MoveDirection.LEFT;
      } else if (e.which === 38) {
        direction = Game.MoveDirection.UP;
      } else if (e.which === 39) {
        direction = Game.MoveDirection.RIGHT;
      } else if (e.which === 40) {
        direction = Game.MoveDirection.DOWN;
      }

      if (direction) {
        this.move(direction);
      }
    });
  }

  addInitialTiles () {
    this.grid.addTile(new Tile(2, { x: 1, y: 2 }));
    this.grid.addTile(new Tile(2, { x: 2, y: 2 }));
    this.grid.addTile(new Tile(4, { x: 2, y: 0 }));
    for (let i = 0; i < this.initialTiles; i++) {
      //this.addRandomTile();
    }
  }

  addRandomTile () {
    let value = Math.random() < this.twoFourDistribution ? 2 : 4;
    let position = this.grid.getRandomAvailableCell();

    this.grid.addTile(new Tile(value, position));
  }

  movesAvailable () {
    return true;
  }

  updateScore (addition) {
    this.score += addition;
    this.board.updateScore(this.score);
  }

  getGridTraversals (direction) {
    let traversals = {
      x: Array.apply(null, { length: this.size }).map(Number.call, Number),
      y: Array.apply(null, { length: this.size }).map(Number.call, Number),
    };

    if (direction === Game.MoveDirection.RIGHT) {
      traversals.x = traversals.x.reverse();
    } else if (direction === Game.MoveDirection.DOWN) {
      traversals.y = traversals.y.reverse();
    }

    return traversals;
  }

  findTargetPosition (tile, direction) {
    let vector = Game.DirectionVector[direction];

    let nextCell, nextTile, targetPosition = tile.position;
    while (true) {
      nextCell = { x: targetPosition.x + vector.x, y: targetPosition.y + vector.y };

      if (!this.grid.isCellWithinBounds(nextCell))
        break;

      nextTile = this.grid.getTile(nextCell);
      if (nextTile !== null && nextTile.value !== tile.value)
        break;

      if (nextTile !== null && nextTile.merged !== null)
        break;

      targetPosition = nextCell;
    }

    return [targetPosition, this.grid.getTile(targetPosition)];
  }

  move (direction) {
    //let previousState = this.grid.clone();
    let didMove = false;

    if (!this.movesAvailable())
      return;

    // Clear merge information from previous move and save each tile's current position as their previous
    for (let y = 0; y < this.grid.size; y++) {
      for (let x = 0; x < this.grid.size; x++) {
        let tile = this.grid.getTile({ x, y });
        if (tile !== null) {
          tile.merged = null;
          tile.saveCurrentPositionAsPrevious();
        }
      }
    }

    let scoreAdditions = 0;

    const traversals = this.getGridTraversals(direction);
    traversals.y.forEach(y => {
      traversals.x.forEach(x => {
        let position = { x, y };
        let tile = this.grid.getTile(position);
        if (tile === null)
          return;

        let [targetPosition, targetTile] = this.findTargetPosition(tile, direction);

        // Don't do anything if the tile shouldn't be moved
        if (targetTile && targetTile.positionEquals(tile.position))
          return;

        if (targetTile) {
          // targetTile exists, merge
          // Prevent merging with self if tile didn't move
          let mergedTile = new Tile(tile.value * 2, targetPosition);
          mergedTile.merged = [tile, targetTile];

          this.grid.addTile(mergedTile);
          this.grid.removeTile(position);

          tile.updatePosition(targetPosition);

          didMove = true;

          scoreAdditions += mergedTile.value;
        } else {
          // targetTile is null, so the cell is empty. Move only.
          this.grid.moveTile(tile, targetPosition);
          didMove = true;
        }
      });
    });

    if (scoreAdditions > 0)
      this.updateScore(scoreAdditions);

    if (didMove) {
      /*if (this.history.length >= this.historySize)
        this.history.shift();

      this.history.push(previousState);*/

      this.addRandomTile();
      this.board.performAction(this.grid);

      if (!this.movesAvailable()) {
        this.gameOver = true;
      }
    }
  }
}
