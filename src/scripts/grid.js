import Tile from "./tile";


export default class Grid {
  constructor (size, state) {
    this.size = size;

    if (state) {
      this.initFromState(state);
    } else {
      this.initEmpty();
    }
  }

  initEmpty () {
    this.cells = [];

    for (let rowIndex = 0; rowIndex < this.size; rowIndex++) {
      let row = this.cells[rowIndex] = [];
      for (let colIndex = 0; colIndex < this.size; colIndex++) {
        row.push(null);
      }
    }
  }

  initFromState (state) {
    this.cells = [];

    let row = this.cells[0] = [];
    state.forEach((i, value) => {
      if (value) {
        row.push(
          new Tile(value, {
            x: Math.floor(i / this.size),
            y: i % this.size,
          })
        );
      } else {
        row.push(null);
      }
    });
  }

  getAvailableCells () {
    let availableCells = [];
    for (let y = 0; y < this.size; y++) {
      for (let x = 0; x < this.size; x++) {
        if (this.cells[y][x] === null) {
          availableCells.push({ x, y });
        }
      }
    }

    return availableCells;
  }

  isCellWithinBounds (cell) {
    return 0 <= cell.x && cell.x < this.size && 0 <= cell.y && cell.y < this.size;
  }

  getRandomAvailableCell () {
    let availableCells = this.getAvailableCells();
    if (!availableCells.length)
      return null;

    return availableCells[Math.floor(Math.random() * availableCells.length)];
  }

  addTile (tile) {
    this.cells[tile.position.y][tile.position.x] = tile;
  }

  getTile (position) {
    return this.cells[position.y][position.x];
  }

  moveTile (tile, newPosition) {
    this.removeTile(tile.position);
    tile.updatePosition(newPosition);
    this.addTile(tile);
  }

  removeTile (position) {
    this.cells[position.y][position.x] = null;
  }
}
