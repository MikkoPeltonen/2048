export default class Board {
  constructor (container, size) {
    this.$container = $(container);
    this.size = size;
    this.score = 0;

    this.generateLayout();
  }

  generateLayout (){
    let html = "";
    html += `
      <div class="header">
        <div class="banner score-banner">
          <span class="banner-title">Score</span>
          <span class="banner-content score">0</span>
        </div>
        <div class="banner">
          <span class="banner-title">Personal Best</span>
          <span class="banner-content personal-best">124 724</span>
        </div>
        <div class="banner">
          <span class="banner-title">All Time Best</span>
          <span class="banner-content all-time-best">237 448</span>
        </div>
      </div>
    `;

    html += `<div class="tiles"></div>`;

    html += `<div class="grid">`;
    for (let row = 0; row < this.size; row++) {
      html += "<div>";
      for (let col = 0; col < this.size; col++) {
        html += `<div class="grid-cell"></div>`
      }
      html += "</div>";
    }
    html += "</div>";

    html += `
    <div class="footer">
      <div class="button new-game">
        New Game
      </div>
    </div>
    `;

    this.$container.html(html);

    this.$grid = this.$container.find(".grid").first();
    this.$tiles = this.$container.find(".tiles").first();
    this.$score = this.$container.find(".score").first();
  }

  performAction (grid) {
    this.clearTiles();
    for (let row = 0; row < grid.size; row++) {
      for (let col = 0; col < grid.size; col++) {
        let tile = grid.cells[row][col];
        if (tile !== null)
          this.drawTile(tile);
      }
    }
  }

  updateScore (newScore) {
    let addition = newScore - this.score;
    this.score = newScore;
    this.$score.html(this.score);

    this.$container.find(".score-addition").remove();

    $(`<span class="score-addition">+${addition}</span>`).appendTo(this.$container.find(".score-banner").first());
  }

  clearTiles () {
    this.$tiles.html("");
  }

  drawTile (tile) {
    let position = tile.previousPosition || tile.position;

    // CSS keyframes on a single property override all of its values. Because the tiles are positioned using
    // transform: translate(...), animating scale or any other property using transform disregards any other
    // values that might already exist (translate). We assign the positioning class to a wrapper element that
    // contains the actual tile that can be animated separately of its position.
    let $tileContainer = $("<div></div>");
    let $tile = $("<div></div>");

    $tileContainer.addClass(`tile-position-${position.y + 1}-${position.x + 1}`);

    $tile.addClass(`tile tile-${tile.value}`);
    $tile.text(tile.value);

    $tileContainer.append($tile);


    if (tile.previousPosition !== null) {
      // If a tile has a previous position set, we start by rendering the tile in the old position
      // and then within window.requestAnimationFrame we change the positioning class to animate the
      // moving of the tile. The callback prevents replacing the class immediately before the tile
      // is rendered in the starting position.
      window.requestAnimationFrame(() => {
        $tileContainer
          .removeClass(`tile-position-${position.y + 1}-${position.x + 1}`)
          .addClass(`tile-position-${tile.position.y + 1}-${tile.position.x + 1}`);
      });
    } else if (tile.merged) {
      // If the tile contains merger information, we start by rendering the to-be-merged tiles in order
      // to animate their paths to this merged tiles. The result is two tiles of value V sliding
      // together to a common position where a new tile of value 2V "pops" in.
      $tileContainer.addClass("tile-merged");

      tile.merged.forEach(tile => {
        this.drawTile(tile);
      });
    } else {
      // If the tile is not a result of a merger nor does it have a previous position associated, it
      // is a newly added random tile. We animate it in place with a fading animation.
      $tileContainer.addClass("tile-new");
    }

    this.$tiles.append($tileContainer);
  }
}
