const webpack = require("webpack");
const path = require("path");

module.exports = {
  mode: "development",
  entry: [
    "./src/scripts/index.js",
  ],
  output: {
    filename: "game.bundle.js",
    path: path.resolve(__dirname, "dist"),
  },
  module: {
    rules: [
      {
        test: /.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /.scss$/,
        exclude: /node_modules/,
        use: [
          "style-loader",
          "css-loader",
          "sass-loader",
        ],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
    }),
  ],
  devServer: {
    inline: true,
    publicPath: "/dist/",
    contentBase: "./src",
    overlay: true,
  },
};
